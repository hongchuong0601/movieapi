import { apiInstance } from "constant";
import { Detail } from "types";



const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_RAP_API
})

export const quanLyRapServices = {
   getDetailList: (movieId: string) => api.get<ApiResponse<Detail[]>>(`/LayThongTinLichChieuPhim?maPhim=${movieId}`)
}