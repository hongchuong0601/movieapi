// file có từ khóa đầu tiên là "use" VSC tự hiểu là hook

import { useSelector } from "react-redux";
import { RootState } from "store";

// lấy thông tin user đã đăng nhập
export const useAuth = () => {
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  return {user}
};
