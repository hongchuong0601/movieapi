// import { Tabs } from "antd";
// import TabPane from "antd/es/tabs/TabPane";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getDetailMovieThunk } from "store/quanLyRap/thunk";

import { CustomCard } from "@tsamantanis/react-glassmorphism";
import "@tsamantanis/react-glassmorphism/dist/index.css";

import { Tabs } from "antd";

// type TabPosition = "left" | "right" | "top" | "bottom";

export const DetailMovieTemplate = () => {
  // const [tabPosition, setTabPosition] = useState<TabPosition>("left");
  // const changeTabPosition = (e: RadioChangeEvent) => {
  //   setTabPosition(e.target.value);
  // };

  const dispatch = useAppDispatch();
  const params = useParams();
  const { movieId } = params;
  const { detailList } = useSelector((state: RootState) => state.quanLyRap);
  console.log("detailList:", detailList);
  useEffect(() => {
    dispatch(getDetailMovieThunk(movieId));
  }, [movieId, dispatch]);
  return (
    <div
      style={{
        backgroundImage: "url(https://picsum.photos/1000)",
        minHeight: "100vh",
      }}
    >
      {detailList?.map((detail) => {
        return (
          <CustomCard
            key={detail.maPhim}
            style={{ paddingTop: 150, minHeight: "100vh" }}
            effectColor="#C780FF" // required
            color="#14AEFF" // default color is white
            blur={20} // default blur value is 10px
            borderRadius={0} // default border radius value is 10px
          >
            <a
              href="#"
              // className="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row  hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700"
            >
              <img
                className="object-cover w-full rounded-t-lg  md:w-60 md:rounded-none md:rounded-l-lg mr-20"
                src=""
                alt=""
              />
              <div className="flex flex-col justify-between p-40 leading-normal">
                <p className="mb-3 font-700 text-gray-900 dark:text-white text-2xl">
                  <span className="font-700 text-white mr-10">Tên phim:</span>{" "}
                  {detail?.tenPhim}
                </p>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400 text-20">
                  <span className="font-700 text-white mr-10">Mô tả:</span>{" "}
                  {/* {detail?.moTa} */}
                </p>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400 text-20">
                  <span className="font-700 text-white mr-10">
                    Ngày khởi chiếu:
                  </span>{" "}
                  {/* {detail?.ngayKhoiChieu} */}
                </p>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400 text-20">
                  <span className="font-700 text-white mr-10">Đánh giá:</span>{" "}
                  {/* {detail?.danhGia} */}
                </p>
              </div>
            </a>
            <>
              {/* <Space style={{ marginBottom: 24 }}>
            Tab position:
            <Radio.Group value={tabPosition} onChange={changeTabPosition}>
              <Radio.Button value="top">top</Radio.Button>
              <Radio.Button value="bottom">bottom</Radio.Button>
              <Radio.Button value="left">left</Radio.Button>
              <Radio.Button value="right">right</Radio.Button>
            </Radio.Group>
          </Space> */}
              <h2 className="font-600 text-white">
                Thông tin chi tiết lịch chiếu phim
              </h2>
              <Tabs
                tabPosition={"left"}
                items={new Array(3).fill(null).map((_, i) => {
                  const id = String(i + 1);
                  return {
                    label: `Tab ${id}`,
                    key: id,
                    children: `Content of Tab ${id}`,
                  };
                })}
              />
            </>
          </CustomCard>
        );
      })}
    </div>
  );
};

export default DetailMovieTemplate;
