import { useNavigate } from "react-router-dom";
import { PATH } from "../../constant/config";
import { useForm, SubmitHandler } from "react-hook-form";
import { LoginSchema, LoginSchemaType } from "schema/LoginSchema";
import { zodResolver } from "@hookform/resolvers/zod/src/zod.js";
// import { quanLyNguoiDungServices } from "service";
import { toast } from "react-toastify";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";
import { Input } from "components/iu";

export const LoginTemplate = () => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });

  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    // phần này đã đẩy qua thunk.ts
    // try {
    //   await quanLyNguoiDungServices.login(value);
    //   toast.success("Đăng nhập thành công");
    //   navigate("/");
    // } catch (error) {
    //   toast.error(error?.response?.data?.content);
    // }
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công");
        navigate("/");
      });
  };

  return (
    <form className="pt-[30px] pb-[60px]" onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40 font-600">Sign In</h2>
      <div className="mt-40">
        {/* <input
          type="text"
          placeholder="userName"
          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          {...register("taiKhoan")}
        />
        <p className="text-red-500">{errors?.taiKhoan?.message}</p> */}
        <Input
          register={register}
          name="taiKhoan"
          error={errors?.taiKhoan?.message}
          placeholder="Tài Khoản"
        />
      </div>
      <div className="mt-40">
        <Input
          register={register}
          name="matKhau"
          error={errors?.matKhau?.message}
          placeholder="Mật Khẩu"
          type="password"
        />
      </div>
      <div className="mt-40">
        <button className="text-white w-full bg-red-700 font-500 rounded-lg text-20 px-5 py-[16px]">
          Sign In
        </button>
        <p className="mt-10 text-white">
          Chưa có tài khoản?{" "}
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => {
              navigate(PATH.register);
            }}
          >
            Đăng ký
          </span>
        </p>
      </div>
    </form>
  );
};

export default LoginTemplate;
