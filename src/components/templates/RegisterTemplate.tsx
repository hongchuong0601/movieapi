import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { quanLyNguoiDungServices } from "service";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { Input } from "components/iu";

export const RegisterTemplate = () => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });

  const navigate = useNavigate();
  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      console.log({ value });
      await quanLyNguoiDungServices.register(value);
      toast.success("Đăng ký thành công");
      navigate(PATH.login);
    } catch (error) {
      console.log("error:", error);
      toast.error(error?.response?.data?.content);
    }
  };

  return (
    <form className="pt-[10px] pb-[20px]" onSubmit={handleSubmit(onSubmit)}>
      <h2 className="text-white text-40 font-600">Register</h2>
      <div className="mt-30">
        {/* <input
          type="text"
          placeholder="Tài Khoản"
          className="outline-none block w-full p-10 text-white border border-white rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          // có zod nên ko cần validate thủ công
          // {...register("taiKhoan", {
          //   required: "Vui lòng nhập tài khoản",
          //   minLength: {
          //     value: 6,
          //     message: 'Nhập từ 6 ký tự',
          //   },
          //   maxLength: {
          //     value: 20,
          //     message: 'Nhập tối đa 20 ký tự',
          //   },
          //   // pattern: {
          //   //   value: ,
          //   //   message: '',
          //   // }
          // })}
          {...register("taiKhoan")}
        />
        <p className="text-red-500">{errors?.taiKhoan?.message as string}</p> */}
        <Input
          register={register}
          name="taiKhoan"
          error={errors?.taiKhoan?.message}
          placeholder="Nhập tài khoản"
          type="text"
        />
      </div>
      <div className="mt-30">
        <Input
          register={register}
          name="matKhau"
          error={errors?.matKhau?.message}
          placeholder="Nhập mật khẩu"
          type="password"
        />
      </div>
      <div className="mt-30">
        <Input
          register={register}
          name="email"
          error={errors?.email?.message}
          placeholder="Nhập mật khẩu"
          type="text"
        />
      </div>
      <div className="mt-30">
        <Input
          register={register}
          name="soDt"
          error={errors?.soDt?.message}
          placeholder="Nhập số điện thoại"
          type="number"
        />
      </div>
      <div className="mt-30">
        <Input
          register={register}
          name="maNhom"
          error={errors?.maNhom?.message}
          placeholder="Nhập mã nhóm"
          type="text"
        />
      </div>
      <div className="mt-30">
        <Input
          register={register}
          name="hoTen"
          error={errors?.hoTen?.message}
          placeholder="Nhập họ tên"
          type="text"
        />
      </div>
      <div className="mt-30">
        <button className="text-white w-full bg-red-700 font-500 rounded-lg text-20 px-5 py-[16px]">
          Sign In
        </button>
      </div>
    </form>
  );
};

export default RegisterTemplate;
