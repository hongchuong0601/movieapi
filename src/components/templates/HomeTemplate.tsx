import { RootState, useAppDispatch } from "store";
import { getMovieListThunk } from "store/quanLyPhim/thunk";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Card, Skeleton } from "components/iu";
import {  generatePath, useNavigate } from "react-router-dom";
import { PATH } from "constant";

export const HomeTemplate = () => {
  const navigate = useNavigate()

  const dispatch = useAppDispatch();
  const { movieList, isFetchingMovieList } = useSelector(
    (state: RootState) => state.quanLyPhim
  );
  console.log("movieList:", movieList);

  // dispatch action thunk call API
  useEffect(() => {
    dispatch(getMovieListThunk());
  }, [dispatch]);

  if (isFetchingMovieList) {
    return (
      <div className="grid grid-cols-4 gap-20">
        {[...Array(16)].map((_, index) => {
          return (
            <Card key={index} className="!w-[250px]">
              <Skeleton.Image active className="!w-full !h-[250px]" />
              <Skeleton.Image active className="!w-full !mt-10" />
              <Skeleton.Image active className="!w-full !mt-10" />
            </Card>
          );
        })}
      </div>
    );
  }

  return (
    <div className="grid grid-cols-4 gap-30 ">
      {movieList?.map((movie) => {
        const path = generatePath(PATH.detail, {
          movieId: movie.maPhim,
        })
        
        return (
          <Card
            key={movie.maPhim}
            hoverable
            style={{ width: 240 }}
            cover={
              <img alt="example" src={movie.hinhAnh} className="w-96 h-96" />
            }
          >
            <Card.Meta
              title={movie.tenPhim}
              description={movie.moTa.substring(0, 50)}
            />
            <button
              className="text-white bg-blue-700 hover:bg-blue-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none  mt-20 ml-5"
              
              type="button"
              onClick={() => {
                navigate(path)
              }}
            >
              Đặt vé
            </button>
          </Card>
        );
      })}
    </div>
  );
};

export default HomeTemplate;
