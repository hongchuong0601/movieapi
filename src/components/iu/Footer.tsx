// import { useNavigate } from "react-router-dom";
import styled from "styled-components";
// import { Avatar, Popover } from ".";
// import { UserOutlined } from "@ant-design/icons";
// import { PATH } from "constant";
// import { useAuth } from "hooks";
// import { useAppDispatch } from "store";
// import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const Footer = () => {
  // const navigate = useNavigate();
  // const { user } = useAuth();
  // const dispatch = useAppDispatch()
  return (
    <FooterS>
      <div className="footer-content">
        <div className="mt-10">
          <h2 className="mb-10 font-700">Cyber Movie</h2>
          <p>Giới thiệu</p>
          <p>Tuyển dụng</p>
          <p>Liên hệ quảng cáo</p>
        </div>
        <div className="mt-10">
          <h2 className="mb-10 font-700">Điều khoản sử dụng</h2>
          <p>Điều khoản chung</p>
          <p>Chính sách thanh toán</p>
          <p>Chính sách bảo mật</p>
        </div>
        <div className="mt-10">
          <h2 className="mb-10 font-700">Kết Nối với chúng tôi</h2>
          <i className="fab fa-facebook mr-10 text-blue-600-600 text-30"></i>
          <i className="fab fa-youtube mr-10 text-red-600 text-30"></i>
          <i className="fab fa-instagram mr-10 text-pink-600 text-30"></i>
          <i className="fab fa-twitter mr-10 text-30"></i>
        </div>
        <div className="mt-10">
          <h2 className="mb-10 font-700">Chăm sóc khách hàng</h2>
          <p>Hotline: 19000000</p>
          <p>Giờ làm việc: 08:00 - 22:00 (24/7)</p>
          <p>Email hỗ trợ: hoidapcyber@gmail.com</p>
        </div>
      </div>
      {/* <div className="footer-content">
        <div className="">
          <NavLink to="" className="mr-40">Cyber Movie</NavLink>
          <NavLink to="" className="mr-40">Chăm sóc khách hàng</NavLink>
          <NavLink to="" className="mr-40">Kết nối với chúng tôi</NavLink>
          {user && (
            <Popover
              content={
                <div className="p-10">
                  <h2 className="font-900">{user?.hoTen}</h2>
                  <hr />
                  <div
                    className="p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                    onClick={() => {
                      navigate(PATH.account);
                    }}
                  >
                    Thông tin tài khoản
                  </div>
                  <div
                    className="p-10 mt-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                    onClick={() => {
                      dispatch(quanLyNguoiDungActions.logOut())
                    }}
                  >
                    Đăng xuất
                  </div>
                </div>
              }
            >
              <Avatar
                className="!ml-40 cursor-pointer !flex !items-center !justify-center !bg-black"
                size={35}
                icon={<UserOutlined />}
              />
            </Popover>
          )}
          {!user && (
            <p
              className="font-600 text-16 ml-40 cursor-pointer"
              onClick={() => {
                navigate(PATH.login);
              }}
            >
              Login
            </p>
          )}
        </div>
      </div> */}
    </FooterS>
  );
};

export default Footer;

const FooterS = styled.footer`
  height: var(--footer-height);
  
  background: white;
  border-top: 5px solid gray;
  .footer-content {
    max-width: var(--max-width);
    margin-top: 40px;
    margin-bottom: 30px !important;
    margin: auto;
    display: flex;
    justify-content: space-between;
    height: 100%;
  }
`;
