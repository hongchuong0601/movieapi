import React from "react";
import { Carousel } from "antd";
import { getBannerListThunk } from "store/quanLyPhim/thunk";
import { useEffect } from "react";
import { RootState, useAppDispatch } from "store";
import { useSelector } from "react-redux";

const contentStyle: React.CSSProperties = {
  height: "350px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

export const HomeCarousel = () => {
  const onChange = (currentSlide: number) => {
    console.log(currentSlide);
  };
  const dispatch = useAppDispatch();
  const { bannerList } = useSelector((state: RootState) => state.quanLyPhim);
  console.log("bannerList:", bannerList);

  useEffect(() => {
    dispatch(getBannerListThunk());
  }, [dispatch]);

  return (
    <div>
      <Carousel afterChange={onChange} className="mt-20">
        {bannerList?.map((banner) => {
          return (
            <div key={banner.maPhim}>
              <div style={contentStyle}>
                <img src={banner.hinhAnh} alt="" className="w-full" />
              </div>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
};

export default HomeCarousel;
