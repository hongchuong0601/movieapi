// file có đuôi name.d.ts chỉ được đùng declare
// khi sử dụng ko cần import
declare type ApiResponse<T> = {
    statusCode: number,
    message: string,
    content: T,
}