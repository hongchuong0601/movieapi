export interface Detail {
    heThongRapChieu: [
      {
        cumRapChieu: [
          {
            lichChieuPhim: [
              {
                maLichChieu: number;
                maRap: number;
                tenRap: string;
                ngayChieuGioChieu: string;
                giaVe: number;
                thoiLuong: number;
              },
            ];
            maCumRap: string;
            tenCumRap: string;
            hinhAnh: string;
            diaChi: string;
          },
        ];
        maHeThongRap: string;
        tenHeThongRap: string;
        logo: string;
      },
    ];
    maPhim: number;
    tenPhim: string;
    biDanh: string;
    trailer: string;
    hinhAnh: string;
    moTa: string;
    maNhom: string;
    hot: boolean;
    dangChieu: boolean;
    sapChieu: boolean;
    ngayKhoiChieu: string;
    danhGia: number;
  }