import { RouteObject } from "react-router-dom";
import { Demo } from "demo";
import { PATH } from "constant";
import { AuthLayout, MainLayout } from "components/layouts";
import { Account, DetailMovie, Home, Login, Register } from "pages";

console.log('PATH', PATH)
export const router: RouteObject[] = [
  {
    path: "/demo",
    element: <Demo />,
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: PATH.account,
        // index: true,
        element: <Account />,
      },
      {
        path: PATH.detail,
        element: <DetailMovie/>
      }
    ],
  },
  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },
    ],
  },
];
