
import HomeTemplate from "components/templates/HomeTemplate";

export const Home = () => {
  return (
    <div>
      <HomeTemplate />
      {/* <HomeTabsTemplate /> */}
    </div>
  );
};

export default Home;
