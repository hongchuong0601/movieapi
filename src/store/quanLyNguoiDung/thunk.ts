import { createAsyncThunk } from "@reduxjs/toolkit";
import { AccountSchemaType } from "schema";
import { LoginSchemaType } from "schema/LoginSchema";
import { quanLyNguoiDungServices } from "service";

// đây là cách tạo async thunk để xử lý API
export const loginThunk = createAsyncThunk(
  "quanLyNguoi/loginThunk",
  async (payload: LoginSchemaType, { rejectWithValue }) => {
    try {
      const data = await quanLyNguoiDungServices.login(payload);
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getUserThunk = createAsyncThunk(
  "quanLyNguoiDung/getUser",
  async (_, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("accessToken");
      if (accessToken) {
        const data = await quanLyNguoiDungServices.getUser();
        return data.data.content;
      }
      return undefined;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);

export const updateUserThunk = createAsyncThunk(
  'quanLyNguoiDung/updateUser',
  async (payload: AccountSchemaType, {rejectWithValue, dispatch}) => {
    try {
      await quanLyNguoiDungServices.updateUser(payload)
      await new Promise((resolve) => setTimeout(resolve, 2000))
      dispatch(getUserThunk())
    } catch (error) {
      rejectWithValue(error)
    }
  }
)
