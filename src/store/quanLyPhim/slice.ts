import { createSlice } from "@reduxjs/toolkit";
import { Banner, Movie } from "types";
import { getBannerListThunk, getMovieListThunk } from "./thunk";

type QuanLyPhimInitialState = {
  movieList: Movie[];
  isFetchingMovieList: boolean;
  bannerList: Banner[];
};

const initialState: QuanLyPhimInitialState = {
  movieList: [],
  isFetchingMovieList: false,
  bannerList: [],
};

const quanLyPhimSlice = createSlice({
  name: "quanLyPhim",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
    .addCase(getMovieListThunk.pending, (state)=>{
      state.isFetchingMovieList = true
    })
    .addCase(getMovieListThunk.fulfilled, (state, { payload }) => {
      state.movieList = payload
      state.isFetchingMovieList = false
    })
    .addCase(getMovieListThunk.rejected, (state)=>{
      state.isFetchingMovieList = false
    })
    
    // .addCase(getBannerListThunk.pending, (state)=>{
    //   state.isFetchingMovieList = true
    // })
    .addCase(getBannerListThunk.fulfilled, (state, {payload})=>{
      state.bannerList = payload
      // state.isFetchingMovieList = false
    })
    // .addCase(getBannerListThunk.rejected, (state)=>{
    //   state.isFetchingMovieList = false
    // })

  },
});

export const { reducer: quanLyPhimReducer, actions: quanLyPhimActions } =
  quanLyPhimSlice;
