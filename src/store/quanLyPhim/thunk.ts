import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhimServices } from "service";

export const getMovieListThunk = createAsyncThunk(
  "quanLyPhim/getMovieListThunk",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyPhimServices.getMovieList();
      await new Promise((resolve)=>setTimeout(resolve, 1000))
      return data.data.content;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getBannerListThunk = createAsyncThunk(
  'quanLyPhim/getBannerListThunk',
  async (_, {rejectWithValue})=> {
    try {
      const data = await quanLyPhimServices.getBannerList()
      return data.data.content
    } catch (error) {
      return rejectWithValue(error);
    }
  }
)


