import { createSlice } from "@reduxjs/toolkit";
import { Detail } from "types";
import { getDetailMovieThunk } from "./thunk";

type quanLyRapInitialState = {
  detailList: Detail[];
  // isFetchingMovieList: boolean,
};
const initialState: quanLyRapInitialState = {
  detailList: [],
  // isFetchingMovieList: false,
};

const quanLyRapSlice = createSlice({
  name: "quanLyRap",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getDetailMovieThunk.fulfilled, (state, { payload }) => {
      state.detailList = payload;
      // state.isFetchingMovieList = false
    });
  },
});

export const { reducer: quanLyRapReducer, actions: quanLyRapActions } =
  quanLyRapSlice;
