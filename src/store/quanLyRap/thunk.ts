import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyRapServices } from "service/quanLyRap";




export const getDetailMovieThunk= createAsyncThunk(
    'quanLyRap/getDetailMovieThunk',
    async (movieId : string, {rejectWithValue}) => {
        console.log("movieId",movieId)
        try {
            const data = await quanLyRapServices.getDetailList(movieId)
            return data.data.content
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)